package tp.monituer;



public class Condition {
    private volatile int waitingCount;

    public void waitCondition(){
        synchronized(this){
            waitingCount++;
            try {
                this.wait();
            } catch (InterruptedException e) {

            System.out.println("GOT THIS ERROR CHIEF: "+e);
            }
        }
    }

    public void signalCondition(){
        synchronized(this){
            if (waitingCount > 0) {
                waitingCount--;
                this.notify();
            }
        }
    }


    public int waitingCount() {
        synchronized(this){
            return waitingCount;
        }
    }


    public boolean empty() {
        synchronized(this){
            return ((waitingCount == 0));
        }
      
    }


    public void signalAllCondition(){
        synchronized(this){
            if (waitingCount > 0) {
                waitingCount= 0;
                this.notifyAll();
            }
        }
    }
}
