package tp.monituer;


public class ResoursePana {
    //THIS IS THE MAXIMIN NUMBER OF AVAILABLE RESOURSES
    private volatile static int NUMBER_RESOURSE_AVAILABLE =10;

    private volatile static Condition available = new Condition();


    
    public void request(int n){
        while (true) {
            synchronized (this) {
                if (n <= NUMBER_RESOURSE_AVAILABLE) {
                    NUMBER_RESOURSE_AVAILABLE -= n;
                    return;
                }
            }
            available.waitCondition();
            synchronized (this) {
                available.signalCondition();
            }
        }
    }


    public void release(int n){
        synchronized(this){
            NUMBER_RESOURSE_AVAILABLE += n;
            available.signalCondition();
        }
    }
}
