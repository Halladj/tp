package tp.semaphore;

public class CarWay1 extends Thread{
    int id=0;

    public CarWay1(int id){
        this.id= id;
    }
    
    public void siesta(){
        try{
            sleep((int)(Math.random()*1500));
        }catch(InterruptedException e){}
    }

    public void entrySection(){
        siesta();
        try {

            LightChanging.getToLight1.acquire();
            LightChanging.light1.acquire();

            System.out.println("|"+this.id+"               |Arrived at Way One  |Way One Is Open      |"+LightChanging.crossingOrder+"                |");
            System.out.println("---------------------------------------------------------------------------");
            //CROSSING COUNTER THINGY
            LightChanging.crossingOrder++;

        } catch (Exception e) {
            System.out.println("SOMETHING WRONG I CAN FEEL IT" +e);
        }

    }

    public void criticalSection(){
        siesta();
        //
        //CROSSING THE ROAD OR SOMETHING
        //
    }

    public void exitSection(){
        LightChanging.light1.release();
        LightChanging.getToLight1.release();
    }

    public void run(){
        entrySection();
        criticalSection();
        exitSection();
    }
}
