package tp.semaphore;

import java.util.concurrent.Semaphore;

//import static java.lang.Thread.sleep;

public class LightChanging extends Thread{
    volatile static boolean whichWayIsOpen=true;


   volatile static Semaphore light1= new Semaphore(1, true);
   volatile static Semaphore light2= new Semaphore(0, true);
   volatile static Semaphore getToLight1= new Semaphore(1, true);
   volatile static Semaphore getToLight2= new Semaphore(1, true);
   volatile static int crossingOrder=0;




    public void siesta(){
        try{
            sleep((int)(Math.random()*3000));
        }catch(InterruptedException e){}
    }


    public void run(){
        System.out.println("|Car Number      |Arrived at Which Way |Which Way Is Open   |Crossing Order");
        System.out.println("---------------------------------------------------------------------------");
        while (true) {
            siesta();
            if (whichWayIsOpen) {
                try {
                    light1.acquire();
                    light2.release();
                    whichWayIsOpen= false;
                } catch (Exception e) {
                    System.out.println("SOMETHING IS WRONG MASTER CHIEF :"+ e);
                }


            }else{
                try {
                    light2.acquire();
                    light1.release();
                    whichWayIsOpen= true;
                } catch (Exception e) {
                    System.out.println("SOMETHING IS WRONG MASTER CHIEF :"+ e);
                }
            }

        }
    }

    public static void main(String[] args) {

        CarWay1 car0= new CarWay1(0);
        CarWay1 car1= new CarWay1(1);
        CarWay1 car2= new CarWay1(2);
        CarWay1 car3= new CarWay1(3);
        CarWay1 car4= new CarWay1(4);
        CarWay1 car5= new CarWay1(5);
        CarWay1 car6= new CarWay1(6);
        CarWay1 car7= new CarWay1(7);
        CarWay1 car8= new CarWay1(8);
        CarWay1 car9= new CarWay1(9);

        CarWay2 car0_2= new CarWay2(0);
        CarWay2 car1_2= new CarWay2(1);
        CarWay2 car2_2= new CarWay2(2);
        CarWay2 car3_2= new CarWay2(3);
        CarWay2 car4_2= new CarWay2(4);
        CarWay2 car5_2= new CarWay2(5);
        CarWay2 car6_2= new CarWay2(6);
        CarWay2 car7_2= new CarWay2(7);
        CarWay2 car8_2= new CarWay2(8);
        CarWay2 car9_2= new CarWay2(9);

        LightChanging light=new LightChanging();
        light.start();

        car0.start();
        car1.start();
        car2.start();
        car3.start();
        car4.start();
        car5.start();
        car6.start();
        car7.start();
        car8.start();
        car9.start();

        car0_2.start();
        car1_2.start();
        car2_2.start();
        car3_2.start();
        car4_2.start();
        car5_2.start();
        car6_2.start();
        car7_2.start();
        car8_2.start();
        car9_2.start();

    }
}
