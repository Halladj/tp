package tp.semaphore;

public class CarWay2 extends Thread{
    int id=0;

    public CarWay2(int id){
        this.id= id;
    }

    public void siesta(){
        try{
            sleep((int)(Math.random()*1500));
        }catch(InterruptedException e){}
    }

    public void entrySection(){
        try {

            LightChanging.getToLight2.acquire();
            LightChanging.light2.acquire();

            System.out.println("|"+this.id+"               |Arrived at Way Two  |Way Two Is Open      |"+LightChanging.crossingOrder+"                |");
            System.out.println("---------------------------------------------------------------------------");

            //CROSSING COUNTER THINGY
            LightChanging.crossingOrder++;

        } catch (Exception e) {
            System.out.println("ERROR NUMBER :"+ e);
        }

    }

    public void criticalSection(){
        siesta();
        //
        //CROSSING THE ROAD OR SOMETHING
        //
    }

    public void exitSection(){
        LightChanging.light2.release();
        LightChanging.getToLight2.release();
    }

    public void run(){
        entrySection();
        criticalSection();
        exitSection();
    }
}
